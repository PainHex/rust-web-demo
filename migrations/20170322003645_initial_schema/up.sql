CREATE TABLE school_subjects (
  id SERIAL PRIMARY KEY,
  subject_name VARCHAR NOT NULL
);

CREATE TABLE question_categories (
  id SERIAL PRIMARY KEY,
  title VARCHAR NOT NULL,
  school_subject_id INTEGER NOT NULL
);

CREATE TABLE questions (
  id SERIAL PRIMARY KEY,
  question VARCHAR NOT NULL,
  question_category_id INTEGER NOT NULL,
  question_hidden_words TEXT[] NOT NULL DEFAULT '{}'
);


