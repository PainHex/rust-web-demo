use iron::prelude::*;
use iron::status;
use hbs::Template;
use diesel::ExpressionMethods;
use diesel::FilterDsl;
use diesel::LimitDsl;
use diesel::LoadDsl;
use rustc_serialize::json::ToJson;
use rustc_serialize::json::Json;
use rustc_serialize::json;

use models::*;
use connection::establish_connection;
use services::category_service;

pub fn index(_: &mut Request) -> IronResult<Response> {
    let mut resp = Response::new();
    resp.set_mut(Template::new("index", category_service::fetch_categories())).set_mut(status::Ok);
    Ok(resp)
}

pub fn test_yourself(_: &mut Request) -> IronResult<Response> {
    let mut resp = Response::new();
    resp.set_mut(Template::new("test_yourself", category_service::fetch_categories())).set_mut(status::Ok);
    Ok(resp)
}
