use diesel::prelude::*;
use diesel::pg::PgConnection;
use diesel;

use models::NewSchoolSubject;
use models::SchoolSubject;

pub fn create_new_school_subject<'a>(conn: &PgConnection, subject_name: &'a str) -> SchoolSubject {
    use schema::school_subjects;

    let new_school_subject = NewSchoolSubject{
        subject_name: subject_name
    };

    diesel::insert(&new_school_subject).into(school_subjects::table)
        .get_result(conn)
        .expect("Error saving new category")
}