use diesel::prelude::*;
use diesel::pg::PgConnection;
use diesel;
use std::collections::HashMap;
use rustc_serialize::json;
use rustc_serialize::json::ToJson;
use rustc_serialize::json::Json;

use models::NewQuestionCategory;
use models::QuestionCategory;
use establish_connection;

pub fn create_new_question_category<'a>(conn: &PgConnection, title: &'a str, subject_id: i32) -> QuestionCategory {
    use schema::question_categories;

    let new_category = NewQuestionCategory {
        title: title,
        school_subject_id: subject_id
    };

    diesel::insert(&new_category).into(question_categories::table)
        .get_result(conn)
        .expect("Error saving new category")
}

pub fn fetch_categories() -> HashMap<String, Json> {
    use schema::question_categories::dsl::*;

    let connection = establish_connection();

    let mut data = HashMap::new();

    data.insert("data".to_string(), question_categories.limit(5)
        .load::<QuestionCategory>(&connection)
        .expect("Error loading posts").to_json());
    data
}