use iron::prelude::*;
use iron::status;
use std::collections::HashMap;
use rustc_serialize::json;
use hbs::Template;
use std::io::Read;

use services::school_subject_service;
use establish_connection;

pub fn create_subject(req: &mut Request) -> IronResult<Response> {
    let mut payload = String::new();
    req.body.read_to_string(&mut payload).unwrap();

    let connection = establish_connection();

    let subject_post_data : HashMap<String, String> = json::decode(&payload).unwrap();

    school_subject_service::create_new_school_subject(&connection, subject_post_data.get("subject_name").unwrap());
    Ok(Response::with(status::Created))
}