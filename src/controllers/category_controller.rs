use iron::prelude::*;
use iron::status;
use std::collections::HashMap;
use rustc_serialize::json;
use hbs::Template;
use std::io::Read;

use establish_connection;
use services::category_service;

pub fn configure_categories(_: &mut Request) -> IronResult<Response> {
    let mut resp = Response::new();
    resp.set_mut(Template::new("test_yourself", ())).set_mut(status::Ok);
    Ok(resp)
}

pub fn create_category(req: &mut Request) -> IronResult<Response> {
    let mut payload = String::new();
    req.body.read_to_string(&mut payload).unwrap();

    let connection = establish_connection();

    let category_post_data : HashMap<String, String> = json::decode(&payload).unwrap();

    category_service::create_new_question_category(&connection, category_post_data.get("title").unwrap(),
                                 category_post_data.get("school_subject_id").unwrap().parse::<i32>().unwrap());
    Ok(Response::with(status::Created))
}