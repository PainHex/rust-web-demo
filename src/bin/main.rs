#![feature(proc_macro)]

extern crate rust_web_demo;

use rust_web_demo::server;

fn main() {
    server::configure().unwrap();
}