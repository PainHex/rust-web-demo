extern crate iron;
extern crate router;
extern crate handlebars;
extern crate handlebars_iron as hbs;
extern crate rustc_serialize;
extern crate maplit;
extern crate mount;
extern crate staticfile;

#[macro_use] extern crate diesel;
#[macro_use] extern crate diesel_codegen;
extern crate dotenv;

pub mod schema;
pub mod models;
pub mod index;
pub mod connection;
pub mod server;
pub mod controllers;
pub mod services;

use diesel::pg::PgConnection;

pub fn establish_connection() -> PgConnection {
    connection::establish_connection()
}