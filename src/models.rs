use super::schema::school_subjects;
use super::schema::question_categories;
use super::schema::questions;
use std::collections::BTreeMap;
use rustc_serialize::json::Json;
use rustc_serialize::json::{ToJson};

/* ===== Question Categories ===== */

#[derive(Identifiable, Queryable, Associations, RustcEncodable, RustcDecodable)]
#[has_many(question_categories)]
#[table_name="school_subjects"]
pub struct SchoolSubject {
    pub id: i32,
    pub subject_name: String
}

#[derive(Insertable)]
#[table_name="school_subjects"]
pub struct NewSchoolSubject<'a> {
    pub subject_name: &'a str
}

/* ===== Question Categories ===== */

#[derive(Identifiable, Queryable, Associations, RustcEncodable, RustcDecodable)]
#[has_many(questions)]
#[belongs_to(SchoolSubject)]
#[table_name="question_categories"]
pub struct QuestionCategory {
    pub id: i32,
    pub title: String,
    pub school_subject_id: i32
}

#[derive(Insertable)]
#[table_name="question_categories"]
pub struct NewQuestionCategory<'a> {
    pub title: &'a str,
    pub school_subject_id: i32
}

/* ===== Question ===== */

#[derive(Identifiable, Queryable, Associations, RustcEncodable, RustcDecodable)]
#[belongs_to(QuestionCategory)]
pub struct Question {
    pub id: i32,
    pub question: String,
    pub question_category_id: i32,
    pub question_hidden_words: Vec<String>
}

#[derive(Insertable)]
#[table_name="questions"]
pub struct NewQuestion<'a> {
    pub question: &'a str,
    pub question_category_id: i32
}



/* ===== ToJson Implementations for HandleBars ===== */

impl ToJson for SchoolSubject {
    fn to_json(&self) -> Json {
        let mut m: BTreeMap<String, Json> = BTreeMap::new();
        m.insert("id".to_string(), self.id.to_json());
        m.insert("subject_name".to_string(), self.subject_name.to_json());
        m.to_json()
    }
}

impl ToJson for QuestionCategory {
    fn to_json(&self) -> Json {
        let mut m: BTreeMap<String, Json> = BTreeMap::new();
        m.insert("id".to_string(), self.id.to_json());
        m.insert("title".to_string(), self.title.to_json());
        m.to_json()
    }
}

impl ToJson for Question {
    fn to_json(&self) -> Json {
        let mut m: BTreeMap<String, Json> = BTreeMap::new();
        m.insert("id".to_string(), self.id.to_json());
        m.insert("question".to_string(), self.question.to_json());
        m.insert("question_hidden_words".to_string(), self.question_hidden_words.to_json());
        m.to_json()
    }
}