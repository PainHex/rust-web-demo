use iron::prelude::*;
use iron::error::HttpResult;
use iron::Listening;
use mount::Mount;
use std::path::Path;
use staticfile::Static;
use router::Router;
use hbs::{HandlebarsEngine, DirectorySource};

use index;
use controllers::*;

pub fn configure() -> HttpResult<Listening> {
    println!("Server running at http://localhost:3001/");
    Iron::new(bind_template_engine(create_router())).http("localhost:3001")
}


fn create_router() -> Router {
    let mut router = Router::new();

    /* ===== Bind Routes ===== */
    router.get("/", index::index, "basic");
    router.get("/index", index::index, "index");
    router.get("/test-yourself", index::test_yourself, "testYourself");
    router.get("/config-categories", category_controller::configure_categories, "configureCategories");

    router.post("/create-school-subject", subject_controller::create_subject, "createSubject");
    router.post("/create-question-category", category_controller::create_category, "createCategory");

    router
}

fn bind_template_engine(router: Router) -> Chain {
    let mut hbse = HandlebarsEngine::new();
    let mut assets_mount = Mount::new();
    assets_mount.mount("/", router);
    assets_mount.mount("/assets", Static::new(Path::new("./resources/assets")));

    hbse.add(Box::new(DirectorySource::new("./resources/templates/", ".hbs")));
    hbse.add(Box::new(DirectorySource::new("./resources/templates/partials", ".hbs")));

    /* ===== Load Templates ==== */
    if let Err(r) = hbse.reload() {
        panic!("{}", r);
    }

    let mut chain = Chain::new(assets_mount);
    chain.link_after(hbse);
    chain
}